<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;
use Throwable;

class CategoriaController extends Controller
{
    public function index()
    {
        try {
            $data = Categoria::all();
            return response()->json([
                "success" => true,
                "data" => $data
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    public function store(Request $request)
    {
        try {
            $categoria = new Categoria();
            $categoria->nombre = trim($request->get('nombre'));
            $categoria->descripcion = trim($request->get('descripcion'));
            $categoria->save();
            return response()->json([
                "success" => true,
                "data" => 'Se ha agregado correctamente.'
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    public function show(Request $request)
    {
        try {
            $data = Categoria::findOrFail($request->get('id'));
            return response()->json([
                "success" => true,
                "data" => $data
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    public function update(Request $request)
    {
        try {
            $categoria = Categoria::findOrFail($request->get('id'));
            $categoria->nombre = trim($request->get('nombre'));
            $categoria->descripcion = trim($request->get('descripcion'));
            $categoria->save();
            return response()->json([
                "success" => true,
                "data" => 'Se actualizo correctamente.'
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        try {
            $categoria = Categoria::findOrFail($request->get('id'));
            $categoria->delete();
            return response()->json([
                "success" => true,
                "data" => 'Se ha eliminado correctamente. '
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }
}
