<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;
use Throwable;

class ProductoController extends Controller
{
    public function index()
    {
        try {
            $data = Producto::all();
            return response()->json([
                "success" => true,
                "data" => $data
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    public function store(Request $request)
    {
        try {
            $producto = new Producto();
            $producto->nombre = trim($request->get('nombre'));
            $producto->precio = $request->get('precio');
            $producto->stock = $request->get('stock');
            $producto->save();
            return response()->json([
                "success" => true,
                "data" => 'Se ha agregado correctamente.'
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    public function show(Request $request)
    {
        try {
            $data = Producto::findOrFail($request->get('id'));
            return response()->json([
                "success" => true,
                "data" => $data
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    public function update(Request $request)
    {
        try {
            $producto = Producto::findOrFail($request->get('id'));
            $producto->nombre = trim($request->get('nombre'));
            $producto->precio = $request->get('precio');
            $producto->stock = $request->get('stock');
            $producto->save();
            return response()->json([
                "success" => true,
                "data" => 'Se actualizo correctamente.'
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        try {
            $producto = Producto::findOrFail($request->get('id'));
            $producto->delete();
            return response()->json([
                "success" => true,
                "data" => 'Se ha eliminado correctamente. '
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }
}
