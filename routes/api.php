<?php

use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ProductoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'categoria'],function(){
    Route::get('list',[CategoriaController::class, 'index']);
    Route::post('add',[CategoriaController::class, 'store']);
    Route::get('view',[CategoriaController::class, 'show']);
    Route::post('update',[CategoriaController::class, 'update']);
    Route::post('destroy',[CategoriaController::class, 'destroy']);
});

Route::group(['prefix' => 'producto'],function(){
    Route::get('list',[ProductoController::class, 'index']);
    Route::post('add',[ProductoController::class, 'store']);
    Route::get('view',[ProductoController::class, 'show']);
    Route::post('update',[ProductoController::class, 'update']);
    Route::post('destroy',[ProductoController::class, 'destroy']);
});
